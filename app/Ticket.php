<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Validator;

class Ticket extends Model {

    const TO_DO = 'TO_DO';
    const IN_PROGRESS = 'IN_PROGRESS';
    const DONE = 'DONE';

    const LOW_PRIORITY = 'LOW';
    const MED_PRIORITY = 'MEDIUM';
    const HIGH_PRIORITY = 'HIGH';


    protected $fillable = [
        'title', 'description', 'status', 'priority', 'user_id', 'lane_id'
    ];

    protected $dates = [];

    public static $createRules = [
        'title' => 'required',
        'description' => 'required',
        'status' => 'in:TO_DO,IN_PROGRESS,DONE',
        'priority' => 'in:LOW,MEDIUM,HIGH',
        'user_id' => 'required'
    ];

    public static $updateRules = [
        'status' => 'in:TO_DO,IN_PROGRESS,DONE',
        'priority' => 'in:LOW,MEDIUM,HIGH'
    ];

    // Relationships

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function lane() {
        return $this->belongsTo(Lane::class);
    }
}
