<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class TicketsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Ticket[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Ticket::all();
    }

    /**
     * @param $ticketId
     * @return mixed
     */
    public function show($ticketId)
    {
        try {
            $ticket = Ticket::findOrFail((int)$ticketId);
            return $ticket;
        } catch (ModelNotFoundException $e) {
            return \response()->json(['message' => 'Not found'], 404);
        } catch (\Exception $e) {
            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }

    }

    /**
     * @param $laneId
     * @return mixed
     */
    public function filter($laneId)
    {
        try {
            $tickets = Ticket::where(['lane_id' => (int)$laneId])->get();
            return $tickets;
        } catch (ModelNotFoundException $e) {
            return \response()->json(['message' => 'Not found'], 404);
        } catch (\Exception $e) {

            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }

    }

    public function search($string) {
        try {
            $tickets = Ticket::where('title', 'like', "%{$string}%")
                ->orWhere('description', 'like', "%{$string}%")
                ->orWhere('status', 'like', "%{$string}%")
                ->orWhere('priority', 'like', "%{$string}%")
                ->orWhere('lane_id', 'like', "%{$string}%")
                ->orWhere('user_id', 'like', "%{$string}%")
                ->get();
            return $tickets;
        } catch (ModelNotFoundException $e) {
            return \response()->json(['message' => 'Not found'], 404);
        } catch (\Exception $e) {

            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, Ticket::$createRules);
            $ticket = Ticket::create($request->all());
            return \response()->json(['message' => 'success', 'id' => $ticket->id], 201);
        } catch (ValidationException $e) {
            $message = $e->getMessage();
            return \response()->json(['message' => $message], 422);
        } catch (\Exception $e) {
            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }
    }

    /**
     * @param Request $request
     * @param $ticketId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $ticketId)
    {
        try {
            $this->validate($request, Ticket::$updateRules);
            $ticket = Ticket::find((int)$ticketId);
            $ticket->update($request->all());
            return response()->json(['message' => 'success'], 202);
        } catch (ValidationException $e) {
            $message = $e->getMessage();
            return response()->json(['message' => $message], 422);
        } catch (\Exception $e) {
            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }
    }

    /**
     * @param $ticketId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($ticketId)
    {
        try {
            if (Ticket::destroy((int)$ticketId)) {
                return response()->json(['message' => 'success'], 202);
            } else {
                return response()->json(['message' => 'Entry not found'], 404);
            }
        } catch (\Exception $e) {
            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }

    }
}
