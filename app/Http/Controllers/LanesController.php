<?php

namespace App\Http\Controllers;

use App\Lane;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LanesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Lane[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Lane::all();
    }

    /**
     * @param $laneId
     * @return mixed
     */
    public function show($laneId)
    {
        try {
            $lane = Lane::findOrFail((int)$laneId);
            return $lane;
        } catch (ModelNotFoundException $e) {
            return \response()->json(['message' => 'Not found'], 404);
        } catch (\Exception $e) {
            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }

    }

    /**
     * @param $laneId
     * @return mixed
     */
    public function filter($laneId)
    {
        try {
            $lane = Lane::findOrFail((int)$laneId);
            return $lane->tickets()->get();
        } catch (ModelNotFoundException $e) {
            return \response()->json(['message' => 'Not found'], 404);
        } catch (\Exception $e) {
            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, Lane::$rules);
            $lane = Lane::create([
                'name' => $request->input('name'),
                'user_id' => Auth::user()->getAuthIdentifier()
            ]);
            return \response()->json(['message' => 'success', 'id' => $lane->id], 201);
        } catch (ValidationException $e) {
            $message = $e->getMessage();
            return \response()->json(['message' => $message], 422);
        } catch (\Exception $e) {
            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }
    }

    /**
     * @param Request $request
     * @param $laneId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $laneId)
    {
        try {

            $lane = Lane::find((int)$laneId);
            $lane->update([
                'name' => $request->input('name'),
                'user_id' => Auth::user()->getAuthIdentifier()
            ]);
            return response()->json(['message' => 'success'], 202);
        } catch (ValidationException $e) {
            $message = $e->getMessage();
            return response()->json(['message' => $message], 422);
        } catch (\Exception $e) {
            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }
    }

    /**
     * @param $laneId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($laneId)
    {
        try {
            if (Lane::destroy((int)$laneId)) {
                return response()->json(['message' => 'success'], 202);
            } else {
                return response()->json(['message' => 'Entry not found'], 404);
            }
        } catch (\Exception $e) {
            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }

    }
}
