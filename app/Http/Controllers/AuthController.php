<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        $user = User::where(['email' => $email])->first();
        if (!Hash::check($password, $user->password)) {
            return response()->json(['error' => 'Invalid credentials'], 401);
        }
        return response()->json(['token' => $user->token], 201);
    }

    public function logout(Request $request)
    {
        $token = $request->header('token');
        $user = User::where('token', $token)->first();
        if (!$user) {
            return response()->json(['error' => 'Invalid credentials'], 401);
        }
        $user->token = User::generateToken();
        $user->save();
    }
}
