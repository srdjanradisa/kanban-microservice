<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except'=>'store']);
    }

    /**
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return User::all();
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function show($userId)
    {
        try {
            $user = User::findOrFail((int)$userId);
            return $user;
        } catch (ModelNotFoundException $e) {
            return \response()->json(['message' => 'Not found'], 404);
        } catch (\Exception $e) {
            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }

    }

    public function search($string) {
        try {
            $tickets = User::where('name', 'like', "%{$string}%")
                ->orWhere('email', 'like', "%{$string}%")
                ->get();
            return $tickets;
        } catch (ModelNotFoundException $e) {
            return \response()->json(['message' => 'Not found'], 404);
        } catch (\Exception $e) {

            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, User::$createRules);
            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password'), ['rounds' => 12]),
                'token' => User::generateToken()
            ]);
            return \response()->json(['message' => 'success', 'id'=>$user->id], 201);
        } catch (ValidationException $e) {
            $message = $e->getMessage();
            return \response()->json(['message' => $message], 422);
        } catch (\Exception $e) {
            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }
    }

    /**
     * @param Request $request
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $userId)
    {
        try {
            $this->validate($request, User::$updateRules);
            $user = User::find((int)$userId);
            $user->update($request->all());
            return response()->json(['message' => 'success'], 202);
        } catch (ValidationException $e) {
            $message = $e->getMessage();
            return response()->json(['message' => $message], 422);
        } catch (\Exception $e) {
            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($userId)
    {
        try {
            if (User::destroy((int)$userId)) {
                return response()->json(['message' => 'success'], 202);
            } else {
                return response()->json(['message' => 'Entry not found'], 404);
            }
        } catch (\Exception $e) {
            return \response()->json(['message' => 'Error occurred please try again later'], 500);
        }

    }
}