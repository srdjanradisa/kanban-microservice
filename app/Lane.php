<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Lane extends Model {

    protected $fillable = ['name','user_id'];

    protected $dates = [];

    public static $rules = [
        'name' => 'required'
    ];

    // Relationships
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function tickets () {
        return $this->hasMany(Ticket::class);
    }

}
