<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'token'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public static $createRules = [
        // Validation rules
        'name' => 'required',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'required|min:6'
    ];

    public static $updateRules = [
        // Validation rules
        'email' => 'email|max:255|unique:users',
        'password' => 'min:6'
    ];

    public static function clearAllUsers() {
        DB::table('users')->delete();
    }

    public static function generateToken() {
        return bin2hex(random_bytes(128));
    }

    public function tickets () {
        return $this->hasMany(Ticket::class);
    }

    public function lanes () {
        return $this->hasMany(Lane::class);
    }




}