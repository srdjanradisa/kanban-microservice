<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Kanban Microservice app';
});

// Tickets routing
$router->get('/tickets', 'TicketsController@index');
$router->get('/tickets/{ticketId}', 'TicketsController@show');
$router->get('/tickets-by-lane/{laneId}', 'TicketsController@filter');
$router->get('/tickets-search/{string}', 'TicketsController@search');
$router->post('/tickets', 'TicketsController@store');
$router->put('/tickets/{ticketId}', 'TicketsController@update');
$router->delete('/tickets/{ticketId}', 'TicketsController@destroy');

//Lanes routing
$router->get('/lanes', 'LanesController@index');
$router->get('/lanes/{laneId}', 'LanesController@show');
$router->get('/lane-tickets/{laneId}', 'LanesController@filter');
$router->post('/lanes', 'LanesController@store');
$router->put('/lanes/{laneId}', 'LanesController@update');
$router->delete('/lanes/{laneId}', 'LanesController@destroy');

// Auth routing
$router->post('/login', 'AuthController@login');
$router->get('/logout', 'AuthController@logout');

// Users routing
$router->get('/users', 'UsersController@index');
$router->get('/users/{userId}', 'UsersController@show');
$router->get('/users-search/{string}', 'UsersController@search');
$router->post('/users', 'UsersController@store');
$router->put('/users/{userId}', 'UsersController@update');
$router->delete('/users/{userId}', 'UsersController@destroy');