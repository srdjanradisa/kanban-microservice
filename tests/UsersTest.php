<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UsersTest extends TestCase
{
    private static $token;

    private static $userId;

    private static $userData = [
        'name' => 'John Doe',
        'email' => 'someuser@somedomain.com',
        'password' => 'test12345'
    ];

    public function testUserCreation()
    {
        $this->json('POST', '/users', self::$userData)
            ->seeJson([
                'message' => 'success',
            ]);
    }

    public function testUserLogin()
    {
        $response = $this->post('/login', [
            'email' => self::$userData['email'],
            'password' => self::$userData['password'],
        ])->response->getContent();
        $this->assertResponseStatus(201);
        $decodedData = json_decode($response, true);
        $this->assertArrayHasKey('token', $decodedData);
        self::$token = $decodedData['token'];
    }

    public function testGetAllUsers()
    {

        $response = $this->get('/users', ['token' => self::$token])->response->getContent();
        $this->assertResponseOk();
        $data = json_decode($response, true);
        $this->assertArrayHasKey(0, $data);
        $this->assertEquals(self::$userData['email'], $data[0]['email']);
        $this->assertEquals(self::$userData['name'], $data[0]['name']);
        self::$userId = $data[0]['id'];
    }

    public function testGetSingleUser()
    {

        $response = $this->get('/users/' . self::$userId, ['token' => self::$token])->response->getContent();
        $this->assertResponseOk();
        $data = json_decode($response, true);
        $this->assertEquals(self::$userData['email'], $data['email']);
        $this->assertEquals(self::$userData['name'], $data['name']);
    }

    public function testSingleUserUpdate()
    {
        $this->json('PUT', '/users/' . self::$userId, ['name' => 'Geronimo'], ['token' => self::$token])
            ->seeJsonEquals([
                'message' => 'success',
            ]);
    }

    public function testUserLogout()
    {
        $this->get('/logout', ['token' => self::$token])->response->getContent();
        $this->assertResponseOk();
    }

    public function testUserDelete()
    {
        // we have to login again
        $response = $this->post('/login', [
            'email' => self::$userData['email'],
            'password' => self::$userData['password'],
        ])->response->getContent();
        $this->assertResponseStatus(201);
        $decodedData = json_decode($response, true);
        $this->assertArrayHasKey('token', $decodedData);
        self::$token = $decodedData['token'];
        // delete
        $this->json('DELETE', '/users/' . self::$userId, [], ['token' => self::$token])
            ->seeJsonEquals([
                'message' => 'success',
            ]);


    }
}
