<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TicketsTest extends TestCase
{
    private static $token;

    private static $userId;

    private static $ticketId;

    private static $laneId;

    private static $userData = [
        'name' => 'John Doe',
        'email' => 'tickets@test.com',
        'password' => 'test12345'
    ];

    private static $ticketData = [
        'title' => 'test ticket',
        'description' => 'test description',
        'status' => \App\Ticket::IN_PROGRESS,
        'priority' => \App\Ticket::HIGH_PRIORITY,
    ];

    private function createAndLoginUser()
    {
        $response = $this->post('/users', self::$userData)->response->getContent();
        $response = json_decode($response, true);
        self::$userId = $response['id'];
        $response = $this->post('/login', [
            'email' => self::$userData['email'],
            'password' => self::$userData['password'],
        ])->response->getContent();
        $decodedData = json_decode($response, true);
        self::$token = $decodedData['token'];
    }

    private function deleteUser()
    {
        $this->json('DELETE', '/users/' . self::$userId, [], ['token' => self::$token]);
    }

    private function createLane()
    {
        $response = $this->json('POST', '/lanes', ['name' => 'test lane'], ['token' => self::$token])->seeJson([
            'message' => 'success',
        ]);
        $decodedResponse = json_decode($response->response->getContent(), true);
        self::$laneId = $decodedResponse['id'];
    }

    private function deleteLane()
    {
        $this->json('DELETE', '/lanes/' . self::$laneId, [], ['token' => self::$token]);
    }

    public function testTicketCreation()
    {
        $this->createAndLoginUser();
        $this->createLane();
        self::$ticketData['lane_id'] = self::$laneId;
        self::$ticketData['user_id'] = self::$userId;
        $response = $this->json('POST', '/tickets', self::$ticketData, ['token' => self::$token]);
        $decodedResponse = json_decode($response->response->getContent(), true);
        self::$ticketId = $decodedResponse['id'];

    }

    public function testGetAllTickets()
    {
        $response = $this->get('/tickets', ['token' => self::$token])->response->getContent();
        $this->assertResponseOk();
        $data = json_decode($response, true);
        $this->assertArrayHasKey(0, $data);
        $this->assertEquals(self::$ticketData['title'], $data[0]['title']);
        $this->assertEquals(self::$ticketData['description'], $data[0]['description']);
        $this->assertEquals(self::$ticketData['status'], $data[0]['status']);
        $this->assertEquals(self::$ticketData['priority'], $data[0]['priority']);
        $this->assertEquals(self::$ticketData['lane_id'], $data[0]['lane_id']);
    }

    public function testGetSingleTicket()
    {
        $response = $this->get('/tickets/' . self::$ticketId, ['token' => self::$token])->response->getContent();
        $this->assertResponseOk();
        $data = json_decode($response, true);
        $this->assertEquals(self::$ticketData['title'], $data['title']);
        $this->assertEquals(self::$ticketData['description'], $data['description']);
        $this->assertEquals(self::$ticketData['status'], $data['status']);
        $this->assertEquals(self::$ticketData['priority'], $data['priority']);
        $this->assertEquals(self::$ticketData['lane_id'], $data['lane_id']);
    }

    public function testSingleTicketUpdate()
    {
        $this->json('PUT', '/tickets/' . self::$ticketId, ['name' => 'test ticket 2'], ['token' => self::$token])
            ->seeJsonEquals([
                'message' => 'success',
            ]);
    }

    public function testSingleTicketDelete()
    {
        $this->json('DELETE', '/tickets/' . self::$ticketId, [], ['token' => self::$token])
            ->seeJsonEquals([
                'message' => 'success',
            ]);

        $this->deleteLane();
        $this->deleteUser();
    }
}
