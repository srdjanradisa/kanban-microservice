<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class LanesTest extends TestCase
{
    private static $token;

    private static $userId;

    private static $laneId;

    private static $userData = [
        'name' => 'John Doe',
        'email' => 'lanes@test.com',
        'password' => 'test12345'
    ];

    private function createAndLoginUser()
    {
        $response = $this->post('/users', self::$userData)->response->getContent();
        $response = json_decode($response, true);
        self::$userId = $response['id'];
        $response = $this->post('/login', [
            'email' => self::$userData['email'],
            'password' => self::$userData['password'],
        ])->response->getContent();
        $decodedData = json_decode($response, true);
        self::$token = $decodedData['token'];
    }

    private function deleteUser()
    {
        $this->json('DELETE', '/users/' . self::$userId, [], ['token' => self::$token]);
    }

    public function testLaneCreation()
    {
        $this->createAndLoginUser();
        $response = $this->json('POST', '/lanes', ['name' => 'test lane'], ['token' => self::$token])->seeJson([
            'message' => 'success',
        ]);
        $decodedResponse = json_decode($response->response->getContent(), true);
        self::$laneId = $decodedResponse['id'];

    }

    public function testGetAllLanes()
    {
        $response = $this->get('/lanes', ['token' => self::$token])->response->getContent();
        $this->assertResponseOk();
        $data = json_decode($response, true);
        $this->assertArrayHasKey(0, $data);
        $this->assertEquals('test lane', $data[0]['name']);
        $this->assertEquals(self::$userId, $data[0]['user_id']);
    }

    public function testGetSingleLane()
    {
        $response = $this->get('/lanes/' . self::$laneId, ['token' => self::$token])->response->getContent();
        $this->assertResponseOk();
        $data = json_decode($response, true);
        $this->assertEquals('test lane', $data['name']);
        $this->assertEquals(self::$userId, $data['user_id']);
    }

    public function testSingleLaneUpdate()
    {
        $this->json('PUT', '/lanes/' . self::$laneId, ['name' => 'test lane 2'], ['token' => self::$token])
            ->seeJsonEquals([
                'message' => 'success',
            ]);
    }

    public function testSingleLaneDelete()
    {
        $this->json('DELETE', '/lanes/' . self::$laneId, [], ['token' => self::$token])
            ->seeJsonEquals([
                'message' => 'success',
            ]);

        $this->deleteUser();
    }
}
